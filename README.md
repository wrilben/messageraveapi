# MessageRave Rest API Access

## Usage and Authorization
To be able to use this API, you must first generate a token.

### Generating Token

`POST Request`

```
Endpoint   https://msg.sturta.com/v1/auth/access
```

```
Payload
{
	"api_key": "Your_API_KEY"
}
```

Success Response

```
{
    "status": "success",
    "response": {
        "id": "1",
        "api_key": "Your_API_Key",
        "jwt": {
            "token": "Your_Authorization_Token",
            "expiration": 1575344285
        }
    },
    "code": 200
}
```

Error Response

```
{
    "status": "error",
    "message": "Invalid API Key",
    "code": 401
}
```


### Authorizing Requests

By default middleware tries to find the token from `Authorization` header. You can authorize each requests by setting
your `Authorization` header value.

eg `Authorization` header = `Bearer Your_Authorization_Token`

#### Authorization Failed Responses

`Token Not Found`

```
{
    "status": "error",
    "message": "Token not found.",
    "code": 401
}
```

`Token Expired`

```
{
    "status": "error",
    "message": "Signature verification failed",
    "code": 401
}
```

## SMS Gateway
Our SMS Gateway is a web service that coordinates and manages the delivery or sending of messages to subscribing endpoints or clients.

In our sms module there are two types of services, `Transactional` and `Promotional`. When you send a message, you can control whether the message is optimized for cost or reliable delivery, and you can specify a sender ID. 

Each SMS message can contain up to 140 bytes, and the character limit depends on the encoding scheme. For example, an SMS message can contain:

160 GSM characters
140 ASCII characters
70 UCS-2 characters


If you publish a message that exceeds the size limit, We send it as multiple messages, each fitting within the size limit. Messages are not cut off in the middle of a word but on whole-word boundaries. The total size limit for a single SMS publish action is 1600 bytes.

### Payload Validations
`For Country`, type country code.

`For Number`, type the phone number to which you want to send the message.

`For Message`, type the message to send.

`For Sender`, type a custom ID that contains up to 11 alphanumeric characters, including at least one letter and no spaces. The sender ID is displayed as the message sender on the receiving device. For example, you can use your business brand to make the message source easier to recognize.

Support for sender IDs varies by country and/or region. For example, messages delivered to U.S. phone numbers will not display the sender ID.

### Transactional Requests
Critical messages that support customer transactions, such as one-time passcodes for multi-factor authentication. We optimize the message delivery to achieve the highest reliability NOTE (Expensive than Promotional).

`POST Request`

```
Endpoint   https://msg.sturta.com/v1/sms/transactional
```

```
Payload
{
	"sender": "Test",
	"country": "+233",
	"message": "The below code can be used to send a SMS",
	"number": "0200000000"
}
```

Success Response

```
{
    "status": "success",
    "response": {
        "sender": "Test",
        "country": "+233",
        "message": "The below code can be used to send a SMS",
        "number": "0243221413"
    },
    "code": 200
}
```

### Promotional Requests
Noncritical messages, such as marketing messages. We optimize the message delivery to incur the lowest cost.

`POST Request`

```
Endpoint   https://msg.sturta.com/v1/sms/promotional
```

```
Payload
{
	"sender": "Test",
	"country": "+233",
	"message": "The below code can be used to send a SMS",
	"number": "0200000000"
}
```

Success Response

```
{
    "status": "success",
    "response": {
        "sender": "Test",
        "country": "+233",
        "message": "The below code can be used to send a SMS",
        "number": "0243221413"
    },
    "code": 200
}
```

### Transaction History
Available soon

### Bulk and Topics
Available soon
